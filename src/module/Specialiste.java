package module;

/** 
 *  Déclarations des imports
 */
import java.util.regex.Pattern;

/**
 * @author DuwigN
 *
 */
public class Specialiste extends Medecin {

	private String specialite;

	/**
	 * Constructeur de ma classe
	 * 
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param codePostal
	 * @param ville
	 * @param telephone
	 * @param email
	 * @param numAgreement
	 * @param specialite
	 * @throws MissMatchException
	 */
	public Specialiste(String nom, String prenom, String adresse, String codePostal, String ville, String telephone,
			String email, int numAgreement, String specialite) throws MissMatchException {
		super(nom, prenom, adresse, codePostal, ville, telephone, email, numAgreement);
		this.specialite = specialite;

	}

	// getter et setter de la classe Achat.
	// comparatifs à l'aide de RegEx dans les exceptions
	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) throws MissMatchException

	{
		if (specialite == null || specialite.trim().isEmpty()) {
			throw new MissMatchException("Saisie de la spécialité incorrecte");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", specialite)) {
			throw new MissMatchException("Veuillez entrer une spécialité valide ");
		} else {
			this.specialite = specialite;
		}
	}
}
