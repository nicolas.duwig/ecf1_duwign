package module;
/** 
 *  Déclarations des imports
 */
import java.util.regex.Pattern;
/**
 * @author DuwigN
 *
 */
public class Medecin extends Personne {
	
	private int numAgreement;


	/**
	 * Constructeur de ma classe
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param codePostal
	 * @param ville
	 * @param telephone
	 * @param email
	 * @param numAgreement
	 * @throws MissMatchException
	 */
	public Medecin(String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String email, int numAgreement) throws MissMatchException {
		super(nom,prenom,adresse,codePostal,ville,telephone,email);
		this.setNumAgreement(numAgreement);
	}


	/**
	 *methode pour un affichage d'une comboBox panel Acceuil
	 *@return : Nom Prenom
	 */
	@Override
	public String toString() {
		return this.getNom() +" "+ getPrenom();
	}





	//getter et setter de la classe Medecin 
	//comparatifs à l'aide de RegEx dans les exceptions

public int getNumAgreement() {
	return numAgreement;
}

public void setNumAgreement(int numAgreement)throws MissMatchException{
	if (String.valueOf(numAgreement)==null||String.valueOf(numAgreement).trim().isEmpty()) {
		throw new MissMatchException("Saisie du numéro d'agrément incorrecte");}
//	if(!Pattern.matches("", String.valueOf(numAgreement))) {
//		throw new MissMatchException("Veuillez entrer un numéro d'agrément valide ");
//	}
	else {
	this.numAgreement = numAgreement;}
	}


}
