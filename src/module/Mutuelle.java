package module;
/** 
 *  Déclarations des imports
 */
import java.util.regex.Pattern;
/**
 * @author DuwigN
 *
 */
public class Mutuelle extends Personne{
	
	private String departement;
	private int taux;

	
	/**
	 * Constructeur de ma classe
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param codePostal
	 * @param ville
	 * @param telephone
	 * @param email
	 * @param departement
	 * @param taux
	 * @throws MissMatchException
	 */
	public Mutuelle (String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String email, String departement, int taux) throws MissMatchException {
		super(nom,prenom,adresse,codePostal,ville,telephone,email);
		this.setDepartement(departement);
		this.setTaux(taux);
	}







	//getter et setter de la classe Mutuelle
	//comparatifs à l'aide de RegEx dans les exceptions

public String getDepartement() {
	return departement;
}


public void setDepartement(String departement)throws MissMatchException{
	if (departement==null||departement.trim().isEmpty()) {
		throw new MissMatchException("Saisie du département incorrecte");}
	if(!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", departement)) {
		throw new MissMatchException("Veuillez entrer un département valide ");
	}
	else {
	this.departement = departement;}
}


public int getTaux() {
	return taux;
}


public void setTaux(int taux) throws MissMatchException{
	if (String.valueOf(taux)==null||String.valueOf(taux).trim().isEmpty()) {
		throw new MissMatchException("Saisie du taux incorrecte");}
//	if(!Pattern.matches("", String.valueOf(taux))) {
//		throw new MissMatchException("Veuillez entrer un taux valide ");
//	}
	else {
	this.taux = taux;}
	}
}

