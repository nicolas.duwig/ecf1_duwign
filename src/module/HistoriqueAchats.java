package module;
/** 
 *  Déclarations des imports
 */
import java.util.ArrayList;
import java.util.List;

/**
 * @author DuwigN
 *
 */

public class HistoriqueAchats {
	
	
	ArrayList<ArrayList<Achat>> listeHistoriqueAchat = new ArrayList<ArrayList<Achat>>();
	
	/**
	*Constructeur de ma classe
	*/
	public HistoriqueAchats(){
		
		
		
	}
	

	
	//getter et setter de la classe HistoriqueAchat
	public ArrayList<ArrayList<Achat>> getListeHistoriqueAchat() {
		return listeHistoriqueAchat;
	}

	public void setListeHistoriqueAchat(ArrayList<ArrayList<Achat>> listeHistoriqueAchat) {
		this.listeHistoriqueAchat = listeHistoriqueAchat ;
	}
	
	//Methode servant à ajouter les listes d'achats à l'historique d'achat
	public void ajouterAchat(ArrayList <Achat> pListeAchat) {
		this.getListeHistoriqueAchat().add(pListeAchat) ;
	}


/**
	*Methode servant à récupérer le nombre de liste d'achat dans l'historique
	*servant pour l'affichage dans le panel HistoriqueDesAchats
	*/
	public int size() {
		// TODO Auto-generated method stub
		int size = listeHistoriqueAchat.size();
		return size;
	}
	
	
}

