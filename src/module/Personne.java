package module;

/** 
 *  Déclarations des imports
 */
import java.util.regex.Pattern;

/**
 * @author DuwigN
 *
 */
public class Personne {

	private String nom;
	private String prenom;
	private String adresse;
	private String codePostal;
	private String ville;
	private String telephone;
	private String email;

	/**
	 * Constructeur de ma classe
	 * 
	 * @param nom
	 * @param prenom
	 * @param adresse
	 * @param codePostal
	 * @param ville
	 * @param telephone
	 * @param email
	 * @throws MissMatchException
	 */
	public Personne(String nom, String prenom, String adresse, String codePostal, String ville, String telephone,
			String email) throws MissMatchException {
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setAdresse(adresse);
		this.setCodePostal(codePostal);
		this.setVille(ville);
		this.setTelephone(telephone);
		this.setEmail(email);
	}

	// getter et setter de la classe Personne
	// comparatifs à l'aide de RegEx dans les exceptions
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) throws MissMatchException {
		if (nom == null || nom.trim().isEmpty()) {
			throw new MissMatchException("Saisie du nom incorrecte");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", nom)) {
			throw new MissMatchException("Veuillez entrer un nom valide ");
		} else {
			this.nom = nom;
		}
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) throws MissMatchException {
		if (prenom == null || prenom.trim().isEmpty()) {
			throw new MissMatchException("Saisie du prenom incorrecte");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", prenom)) {
			throw new MissMatchException("Veuillez entrer un prenom valide ");
		} else {
			this.prenom = prenom;
		}
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) throws MissMatchException {
		if (adresse == null || adresse.trim().isEmpty()) {
			throw new MissMatchException("Saisie de l'adresse incorrecte");
		}
		if (!Pattern.matches("([0-9-]*) ?([\\p{L}*,\\. '-]*) ?([0-9 ]*) ?([\\p{L}*,\\. '-]*)", adresse)) {
			throw new MissMatchException("Veuillez entrer une adresse valide");
		} else {
			this.adresse = adresse;
		}
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) throws MissMatchException {
		if (codePostal == null || codePostal.trim().isEmpty()) {
			throw new MissMatchException("Saisie du code Postal incorrecte");
		}
		if (!Pattern.matches("(\\d{2}[ ]?\\d{3})", codePostal)) {
			throw new MissMatchException("Veuillez entrer un code Postal valide");
		} else {
			this.codePostal = codePostal;
		}
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) throws MissMatchException {
		if (ville == null || ville.trim().isEmpty()) {
			throw new MissMatchException("Saisie de la ville incorrecte");
		}
		if (!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", ville)) {
			throw new MissMatchException("Veuillez entrer une ville valide");
		} else {
			this.ville = ville;
		}
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) throws MissMatchException {
		if (telephone == null || telephone.trim().isEmpty()) {
			throw new MissMatchException("Saisie du numéro de téléphone incorrecte");
		}
		if (!Pattern.matches("^(\\+33|0|0033)[1-9](\\d{2}){4}$", telephone)) {
			throw new MissMatchException("Veuillez entrer un numéro de téléphone valide");
		} else {
			this.telephone = telephone;
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws MissMatchException {
		if (email == null || email.trim().isEmpty()) {
			throw new MissMatchException("Saisie de l'adresse Email incorrecte");
		}
		if (!Pattern.matches("^[a-zA-Z0-9]+[-._]*[a-zA-Z0-9]+@[a-zA-Z0-9]+[-]*[a-zA-Z0-9]*.[a-zA-Z0-9]+$", email)) {
			throw new MissMatchException("Veuillez entrer une adresse mail valide");
		} else {
			this.email = email;
		}
	}

}