package module;
/** 
 *  Déclarations des imports
 */
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Pattern;

import vue.*;
/**
 * @author DuwigN
 *
 */
public class Achat {
	
	private Medicament medicament;
	private Client client;
	private LocalDate dateAchat;
	private String quantite;
	
	
	/**
	 * Constructeur de ma classe
	 */
	/**
	 * Constructeur de ma classe
	 * @param pMedicament
	 * @param pClient
	 * @param quantite
	 */
	public Achat(Medicament pMedicament, Client pClient, String quantite) {
		this.setMedicament(pMedicament);
		this.setClient(pClient);
		this.dateAchat = LocalDate.now();
		this.quantite = quantite;
		
	}


	

//getter et setter de la classe Achat. 
//comparatifs à l'aide de RegEx dans les exceptions
	public Medicament getMedicament() {
		return medicament;
	}


	public void setMedicament(Medicament medicament) {
		this.medicament = medicament;
	}


	public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}


	public LocalDate getDateAchat() {
		return dateAchat;
	}


	public void setDateAchat(LocalDate dateAchat) throws MissMatchException{
		if (dateAchat==null||dateAchat.toString().trim().isEmpty()) {
			throw new MissMatchException("Saisie de la date d'achat incorrecte");}
		if(!Pattern.matches("^\\d{4}-\\d{2}-\\d{2}", dateAchat.toString())) {
			throw new MissMatchException("Veuillez entrer un format de date valide : année/mois/jour ");
		}
		else {
		this.dateAchat = dateAchat;}
	}
	
	
	public String getQuantite() {
		return quantite;
	}


	public void setQuantite(String quantite) throws MissMatchException{
		if (quantite==null||quantite.trim().isEmpty()) {
			throw new MissMatchException("Saisie de la quantite incorrecte");}
		if(!Pattern.matches("^\\d+$", quantite)) {
			throw new MissMatchException("Veuillez entrer une quantite valide ");
		}
		else {
		this.quantite = quantite;}
	}
	

}

