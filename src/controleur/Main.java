package controleur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDate;

import module.Client;
import module.HistoriqueAchats;
import module.Liste;
import module.Medecin;
import module.Medicament;
import module.MissMatchException;
import module.Specialiste;
/** 
 *  Déclarations des imports
 */
import vue.Principale;

/**
 * @author DuwigN
 *
 */
public class Main {

	/**
	 * Methode principale
	 * 
	 * @param args
	 */

	public static void main(String[] args) throws MissMatchException {
		// TODO Auto-generated method stub

		String BDD = "Sparadonnees";
		String url = "jdbc:mysql://localhost:3306/" + BDD;
		String user = "root";
		String passwd = "Sdv88100NAP*";

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, passwd);
			System.out.println("Connecter");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}
		;

		// instenciation des dates de naissances des clients
		LocalDate dateNaissanceJean = LocalDate.of(2005, 6, 8);
		LocalDate dateNaissanceMax = LocalDate.of(2005, 6, 8);
		LocalDate dateNaissanceSophie = LocalDate.of(2005, 6, 8);
		LocalDate dateNaissanceIsabelle = LocalDate.of(2005, 6, 8);

		// instenciation des clients
		Client Jean = new Client("Dupont", "Jean", "8 rue du près", "54000", "Nancy", "0601020304",
				"JeanDupont@gmail.com", "111111111111111", dateNaissanceJean, "466");
		Client Max = new Client("Dubois", "Max", "8 rue du près", "54630", "Nancy", "0601020304",
				"JeanDupont@gmail.com", "111111111111111", dateNaissanceMax, "466");
		Client Sophie = new Client("Martin", "Sophie", "8 rue du près", "55810", "Nancy", "0601020304",
				"JeanDupont@gmail.com", "111111111111111", dateNaissanceSophie, "466");
		Client Isabelle = new Client("Petit", "Isabelle", "8 rue du près", "54587", "Nancy", "0601020304",
				"JeanDupont@gmail.com", "111111111111111", dateNaissanceIsabelle, "466");

		// enregistrement des clients dans la liste de client
		Liste listes = new Liste();
		listes.ajouterClient(Jean);
		listes.ajouterClient(Max);
		listes.ajouterClient(Sophie);
		listes.ajouterClient(Isabelle);

		// instenciation des médecins
		Medecin Luc = new Medecin("Durant", "Luc", "5 rue saligo", "65320", "quichonpré", "0653965545",
				"LucBlup@gmail.com", 58810);
		Medecin Jacques = new Medecin("Marchand", "Jacques", "8 rue du près", "88100", "Saint-dié", "0624735772",
				"Aroussel@hotmail.fr", 755);
		Medecin Claire = new Medecin("Muller", "Claire", "8 rue du près", "88100", "Saint-dié", "0624735772",
				"Aroussel@hotmail.fr", 755);

		// enregistrement des Medecin dans la liste de medecin
		listes.ajouterMedecin(Luc);
		listes.ajouterMedecin(Jacques);
		listes.ajouterMedecin(Claire);

		// instenciation des spécialistes et ajout dans la liste des specialistes
		Specialiste Annick = new Specialiste("Roussel", "Annick", "8 rue du près", "88100", "Saint-dié", "0624735772",
				"Aroussel@hotmail.fr", 755, "cardiologue");
		listes.ajouterSpecialiste(Annick);

		// instenciation des médicaments et ajout de ceux-ci dans la liste medicament
		Medicament medicPara = new Medicament("Paracétamol", "Antalgique", 2.18, dateNaissanceJean, 500);
		Medicament medicDafa = new Medicament("Dafalgan", "Antalgique", 4.50, dateNaissanceJean, 650);
		Medicament medicIbu = new Medicament("Ibuprofène", "Antalgique", 3.20, dateNaissanceJean, 750);
		listes.ajouterMedicament(medicPara);
		listes.ajouterMedicament(medicDafa);
		listes.ajouterMedicament(medicIbu);

		// instenciation de la liste d'historique d'achat
		HistoriqueAchats historiqueAchat = new HistoriqueAchats();
		// création de la première frame, début de l'utilisation utilisateur.
		Principale frame = new Principale(listes, historiqueAchat);
		frame.setVisible(true);
	}

}
