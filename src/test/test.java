package test;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import module.MissMatchException;
import module.Personne;

public class test {

	private static Personne pers;

	@BeforeAll
	static void initialize() {
		try {
			pers = new Personne("Dupont", "Jean", "8 rue du près", "54000", "Nancy", "0601020304",
					"JeanDupont@gmail.com");
		} catch (MissMatchException mme) {
			mme.printStackTrace();
		}
	}

	@Test
	@DisplayName("Constructeur correct")
	void testPersonneIntStringStringStringStringStringIntIntInt() {
		assertTrue(pers.getNom() == "Dupont");
		assertTrue(pers.getPrenom() == "Jean");
		assertTrue(pers.getAdresse() == "8 rue du près");
		assertTrue(pers.getCodePostal() == "54000");
		assertTrue(pers.getVille() == "Nancy");
		assertTrue(pers.getTelephone() == "0601020304");
		assertTrue(pers.getEmail() == "JeanDupont@gmail.com");

	}

	@Test
	@DisplayName("Nom de la personne avec champ null")
	void testSetNomPersonneNull() {
		try {
			pers.setNom(null);
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du nom incorrecte"));
		}
	}

	@Test
	@DisplayName("Nom de la personne avec champ vide")
	void testSetNomPersonneEmpty() {
		try {
			pers.setNom("");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du nom incorrecte"));
		}
	}

	@Test
	@DisplayName("Nom de la personne faussement ecrit")
	void testSetNomPersonneFalse() {
		try {
			pers.setNom("858");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Veuillez entrer un nom valide "));
		}
	}

	@Test
	@DisplayName("Prenom de la personne avec champ null")
	void testSetPrenomPersonneNull() {
		try {
			pers.setPrenom(null);
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du prenom incorrecte"));
		}
	}

	@Test
	@DisplayName("Prenom de la personne avec champ vide")
	void testSetPrenomPersonneEmpty() {
		try {
			pers.setPrenom("");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du prenom incorrecte"));
		}
	}

	@Test
	@DisplayName("Prenom de la personne faussement ecrit")
	void testSetPrenomPersonneFalse() {
		try {
			pers.setPrenom("858");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Veuillez entrer un prenom valide "));
		}
	}

	@Test
	@DisplayName("Adresse de la personne avec champ null")
	void testSetAdressePersonneNull() {
		try {
			pers.setAdresse(null);
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie de l'adresse incorrecte"));
		}
	}

	@Test
	@DisplayName("Adresse de la personne avec champ vide")
	void testSetAdressePersonneEmpty() {
		try {
			pers.setAdresse("");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie de l'adresse incorrecte"));
		}
	}

	@Test
	@DisplayName("Adresse de la personne faussement ecrit")
	void testSetAdressePersonneFalse() {
		try {
			pers.setAdresse("!");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Veuillez entrer une adresse valide"));
		}
	}

	@Test
	@DisplayName("Code Postal de la personne avec champ null")
	void testSetCodePostalPersonneNull() {
		try {
			pers.setCodePostal(null);
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du code Postal incorrecte"));
		}
	}

	@Test
	@DisplayName("Code Postal de la personne avec champ vide")
	void testSetCodePostalPersonneEmpty() {
		try {
			pers.setCodePostal("");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du code Postal incorrecte"));
		}
	}

	@Test
	@DisplayName("Code Postal de la personne faussement ecrit")
	void testSetCodePostalPersonneFalse() {
		try {
			pers.setCodePostal("a");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Veuillez entrer un code Postal valide"));
		}
	}

	@Test
	@DisplayName("Ville de la personne avec champ null")
	void testSetVillePersonneNull() {
		try {
			pers.setVille(null);
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie de la ville incorrecte"));
		}
	}

	@Test
	@DisplayName("Ville de la personne avec champ vide")
	void testSetVillePersonneEmpty() {
		try {
			pers.setVille("");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie de la ville incorrecte"));
		}
	}

	@Test
	@DisplayName("Ville de la personne faussement ecrit")
	void testSetVillePersonneFalse() {
		try {
			pers.setVille("8");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Veuillez entrer une ville valide"));
		}
	}

	@Test
	@DisplayName("Telephone de la personne avec champ null")
	void testSetTelephonePersonneNull() {
		try {
			pers.setTelephone(null);
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du numéro de téléphone incorrecte"));
		}
	}

	@Test
	@DisplayName("Telephone de la personne avec champ vide")
	void testSetTelephonePersonneEmpty() {
		try {
			pers.setTelephone("");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie du numéro de téléphone incorrecte"));
		}
	}

	@Test
	@DisplayName("Telephone de la personne faussement ecrit")
	void testSetTelephonePersonneFalse() {
		try {
			pers.setTelephone("8");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Veuillez entrer un numéro de téléphone valide"));
		}
	}

	@Test
	@DisplayName("Email de la personne avec champ null")
	void testSetEmailPersonneNull() {
		try {
			pers.setEmail(null);
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie de l'adresse Email incorrecte"));
		}
	}

	@Test
	@DisplayName("Email de la personne avec champ vide")
	void testSetEmailPersonneEmpty() {
		try {
			pers.setEmail("");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Saisie de l'adresse Email incorrecte"));
		}
	}

	@Test
	@DisplayName("Email de la personne faussement ecrit")
	void testSetEmailPersonneFalse() {
		try {
			pers.setEmail("8");
		} catch (MissMatchException mme) {
			assert (mme.getMessage().equals("Veuillez entrer une adresse mail valide"));
		}
	}
}
