package vue;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/** 
 *  Déclarations des imports
 */
import module.Achat;
import module.Client;
import module.HistoriqueAchats;
import module.Liste;
import module.Medicament;

/**
 * @author DuwigN
 *
 */
public class AchatDirect extends JPanel {
	private JTextField quantite;
	private double prixTotal = 0;

	/**
	 * Constructeur du panel AchatDirect
	 * 
	 * @param pliste
	 * @param pHistoriqueAchats
	 */

	public AchatDirect(Liste pliste, HistoriqueAchats pHistoriqueAchats) {
//		fen.setVisible(false);
		setBounds(new Rectangle(0, 140, 684, 521));
		setLayout(null);

		JLabel lblTitreAchatDirect = new JLabel("Achat direct");
		lblTitreAchatDirect.setBounds(291, 11, 147, 29);
		add(lblTitreAchatDirect);
		lblTitreAchatDirect.setFont(new Font("Serif", Font.BOLD, 20));

		JLabel lblDonneeClient = new JLabel("Données du client :");
		lblDonneeClient.setBounds(24, 113, 147, 29);
		add(lblDonneeClient);

		JLabel lblNom = new JLabel("Nom :");
		lblNom.setBounds(10, 149, 89, 29);
		add(lblNom);

		JButton btnConfirmer = new JButton("Confirmer");
		btnConfirmer.setBounds(10, 267, 100, 23);
		add(btnConfirmer);

		JLabel lblNomVisu = new JLabel("");
		lblNomVisu.setBounds(64, 149, 89, 29);
		add(lblNomVisu);
		lblNomVisu.setVisible(false);

		JLabel lblPrenomVisu = new JLabel("");
		lblPrenomVisu.setBounds(64, 204, 89, 29);
		add(lblPrenomVisu);
		lblPrenomVisu.setVisible(false);

		JLabel lblDonneeMed = new JLabel("Données du médicament :");
		lblDonneeMed.setBounds(270, 113, 147, 29);
		add(lblDonneeMed);
		lblDonneeMed.setVisible(false);

		JLabel lblNomMed = new JLabel("Nom :");
		lblNomMed.setBounds(237, 149, 89, 29);
		add(lblNomMed);
		lblNomMed.setVisible(false);

		JLabel lblQuantite = new JLabel("Quantité :");
		lblQuantite.setBounds(237, 204, 89, 29);
		add(lblQuantite);
		lblQuantite.setVisible(false);

		quantite = new JTextField();
		quantite.addContainerListener(new ContainerAdapter() {
			@Override
			public void componentRemoved(ContainerEvent e) {
			}
		});
		quantite.setColumns(10);
		quantite.setBounds(299, 208, 42, 20);
		add(quantite);
		quantite.setVisible(false);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.setBounds(241, 267, 100, 23);
		add(btnAjouter);
		btnAjouter.setVisible(false);

		JButton btnConfirmerAchat = new JButton("Confirmer l'achat");
		btnConfirmerAchat.setBounds(10, 487, 161, 23);
		add(btnConfirmerAchat);
		btnConfirmerAchat.setVisible(false);

		DefaultListModel<String> model = new DefaultListModel<>();
		JList<String> listMed = new JList<>(model);
		listMed.setBounds(462, 95, 194, 297);
		add(listMed);
		listMed.setVisible(false);

		JLabel lblPanier = new JLabel("Panier :");
		lblPanier.setBounds(462, 67, 147, 29);
		add(lblPanier);
		lblPanier.setVisible(false);

		JComboBox<Client> comboBoxClient = new JComboBox<>();
		for (Client client : pliste.getListeClient())
			comboBoxClient.addItem(client);
		comboBoxClient.setBounds(64, 153, 107, 22);
		add(comboBoxClient);

		JComboBox<Medicament> comboBoxMedicament = new JComboBox<>();
		for (Medicament medicament : pliste.getListeMedicament())
			comboBoxMedicament.addItem(medicament);
		comboBoxMedicament.setBounds(296, 152, 129, 22);
		add(comboBoxMedicament);
		comboBoxMedicament.setVisible(false);

		JLabel lblPrixTotal = new JLabel("Prix total :");
		lblPrixTotal.setBounds(462, 391, 194, 20);
		add(lblPrixTotal);
		lblPrixTotal.setVisible(false);

		JPanel panAchatDirect = new JPanel();
		panAchatDirect.setBounds(0, 140, 684, 521);
		panAchatDirect.setLayout(null);

		btnConfirmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnConfirmer.setVisible(false);
				lblNomVisu.setVisible(true);
				lblPrenomVisu.setVisible(true);
				lblDonneeMed.setVisible(true);
				lblNomMed.setVisible(true);
				lblQuantite.setVisible(true);
				comboBoxClient.getSelectedItem();

				quantite.setVisible(true);
				listMed.setVisible(true);
				comboBoxClient.setVisible(false);
				lblNomVisu.setText(((Client) comboBoxClient.getSelectedItem()).getNom() + " "
						+ ((Client) comboBoxClient.getSelectedItem()).getPrenom());
				comboBoxMedicament.setVisible(true);
				lblPanier.setVisible(true);
				lblPrixTotal.setVisible(true);
			}
		});

		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Achat achat = new Achat((Medicament) comboBoxMedicament.getSelectedItem(),
						(Client) comboBoxClient.getSelectedItem(), quantite.getText());
				pliste.getListeAchat().add(achat);
				double prix = Math.round(((Medicament) comboBoxMedicament.getSelectedItem()).getPrix()
						* Integer.parseInt(quantite.getText()) * 100.0) / 100.0;
				model.addElement((Medicament) comboBoxMedicament.getSelectedItem() + " : " + quantite.getText()
						+ "       " + prix + " €");
				prixTotal = prixTotal + prix;
				lblPrixTotal.setText("Prix total : " + Math.round(prixTotal * 100.0) / 100.0);
				quantite.setText("");
				btnAjouter.setVisible(false);
				btnConfirmerAchat.setVisible(true);
			}
		});

		btnConfirmerAchat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				int INFORMATION_MESSAGE = 0;
				String message = new String("L'achat a été enregistré");
				String titreDialogue = new String("Confirmation d'achat");
				JOptionPane.showMessageDialog(null, message, titreDialogue, INFORMATION_MESSAGE);
				pHistoriqueAchats.ajouterAchat(pliste.getListeAchat());

				btnConfirmer.setVisible(true);
				btnConfirmerAchat.setVisible(false);
				lblNomVisu.setVisible(false);
				lblPrenomVisu.setVisible(false);
				lblDonneeMed.setVisible(false);
				lblNomMed.setVisible(false);
				lblQuantite.setVisible(false);
				listMed.setVisible(false);
				comboBoxClient.setVisible(true);
				comboBoxMedicament.setVisible(false);
				lblPanier.setVisible(false);
				lblPrixTotal.setVisible(false);
				quantite.setVisible(false);
				model.clear();
				lblPrixTotal.setText("");
				prixTotal = 0;
			}
		});

		quantite.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				btnAjouter.setVisible(true);
				char c = e.getKeyChar();
				if (((c < '0') || (c > '9')) && (c != KeyEvent.VK_BACK_SPACE)) {
					e.consume(); // ignorer l'événement
				}
			}
		});

	}
}
