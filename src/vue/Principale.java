package vue;

/** 
 *  Déclarations des imports
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import module.Client;
import module.HistoriqueAchats;
import module.Liste;
import module.Medecin;

/**
 * @author DuwigN
 *
 */
public class Principale extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Principale frame = new Principale();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Constructeur de ma frame principale
	 * 
	 * @param pliste
	 * @param pHistoriqueAchats
	 */
	public Principale(Liste pliste, HistoriqueAchats pHistoriqueAchats) {

		// création de la frame principale
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		contentPane.setLayout(null);

		// appel du panel AchatDirect
		AchatDirect achatDirect = new AchatDirect(pliste, pHistoriqueAchats);
		achatDirect.setVisible(false);

		// Création d'un panel de présentation (nom de la pharmacie ect) fixe
		JPanel panLogo = new JPanel();
		panLogo.setBackground(new Color(255, 255, 255));
		panLogo.setBounds(0, 0, 684, 141);
		getContentPane().add(panLogo);
		panLogo.setLayout(null);

		JLabel lblLogo = new JLabel("Logo");
		lblLogo.setIcon(new ImageIcon(Principale.class.getResource("/media/Logo-Pharmacie.jpg")));
		lblLogo.setBounds(534, 11, 108, 108);
		panLogo.add(lblLogo);

		JLabel lblTitre = new JLabel("Pharmacie Sparadrap", SwingConstants.CENTER);
		lblTitre.setBounds(-74, 41, 684, 48);
		panLogo.add(lblTitre);
		lblTitre.setFont(new Font("Serif", Font.BOLD, 40));

		JLabel lblTitreAchat = new JLabel("Choisir le type d'achat");
		lblTitreAchat.setBounds(254, 140, 201, 32);
		panLogo.add(lblTitreAchat);
		lblTitreAchat.setFont(new Font("Serif", Font.BOLD, 20));

		// Boutons et panel pour la page d'acceuil
		JPanel panAcceuil = new JPanel();
		panAcceuil.setBounds(0, 129, 684, 521);
		getContentPane().add(panAcceuil);
		panAcceuil.setLayout(null);

		JButton btnAchats = new JButton("Achats");
		btnAchats.setBounds(300, 72, 89, 23);
		panAcceuil.add(btnAchats);

		JButton btnHistAchats = new JButton("Historique des achats");
		btnHistAchats.setBounds(266, 173, 182, 23);
		panAcceuil.add(btnHistAchats);

//		JButton btnHistOrd = new JButton("Historique des ordonnances");

//		btnHistOrd.setBounds(263, 265, 185, 23);
//		panAcceuil.add(btnHistOrd);
		JComboBox<Medecin> comboBoxOrdMedecin = new JComboBox<>();
		for (Medecin medecin : pliste.getListeMedecin())
			comboBoxOrdMedecin.addItem(medecin);
		comboBoxOrdMedecin.setBounds(263, 265, 185, 23);
		panAcceuil.add(comboBoxOrdMedecin);

		JLabel lblHistOrd = new JLabel("Historique des ordonnances : ");
		lblHistOrd.setBounds(80, 267, 185, 19);
		panAcceuil.add(lblHistOrd);

		JLabel lblChoixMedecin = new JLabel("Choisir un medecin");
		lblChoixMedecin.setBounds(462, 269, 119, 14);
		panAcceuil.add(lblChoixMedecin);

		JLabel lblDetailClient = new JLabel("Detail du client : ");
		lblDetailClient.setBounds(80, 367, 185, 19);
		panAcceuil.add(lblDetailClient);

		JComboBox<Client> comboBoxDetailClient = new JComboBox<>();
		for (Client client : pliste.getListeClient())
			comboBoxDetailClient.addItem(client);
		comboBoxDetailClient.setBounds(266, 365, 182, 23);
		panAcceuil.add(comboBoxDetailClient);

		// Boutons pour le premier visuel d'achat
		JButton btnAchatDirect = new JButton("Achat direct");
		btnAchatDirect.setBounds(276, 135, 146, 23);
		panAcceuil.add(btnAchatDirect);
		btnAchatDirect.setVisible(false);

		JButton btnAchatOrd = new JButton("Achat via ordonnance");
		btnAchatOrd.setBounds(276, 232, 146, 23);
		panAcceuil.add(btnAchatOrd);
		btnAchatOrd.setVisible(false);

		// Boutons et label pour le visuel premier d'achat via ordonnance
		JButton btnLeMdecinTraitant = new JButton("Le médecin traitant");
		btnLeMdecinTraitant.setBounds(80, 193, 171, 23);
		panAcceuil.add(btnLeMdecinTraitant);
		btnLeMdecinTraitant.setVisible(false);

		JButton btnUnSpecialiste = new JButton("Un spécialiste");
		btnUnSpecialiste.setBounds(423, 193, 171, 23);
		panAcceuil.add(btnUnSpecialiste);
		btnUnSpecialiste.setVisible(false);

		JLabel lblDelivrance = new JLabel("Délivrée par :");
		lblDelivrance.setFont(new Font("Serif", Font.PLAIN, 15));
		lblDelivrance.setBounds(293, 92, 129, 32);
		panAcceuil.add(lblDelivrance);
		lblDelivrance.setVisible(false);

		// création bouton quitter et Retour
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setBounds(585, 487, 89, 23);
		panAcceuil.add(btnQuitter);

		JButton btnRetour = new JButton("Retour");
		btnRetour.setBounds(464, 487, 100, 23);
		getContentPane().add(btnRetour);
		btnRetour.setVisible(false);

		// actionListener pour passer au panel achat
		btnAchats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAchats.setVisible(false);
				btnHistAchats.setVisible(false);
				comboBoxOrdMedecin.setVisible(false);
				comboBoxDetailClient.setVisible(false);
				lblHistOrd.setVisible(false);
				lblChoixMedecin.setVisible(false);
				lblDetailClient.setVisible(false);
				btnAchatDirect.setVisible(true);
				btnAchatOrd.setVisible(true);
			}
		});

		// actionListener pour passer au panel d'achat direct
		btnAchatDirect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panAcceuil.setVisible(false);
				getContentPane().add(achatDirect);
				achatDirect.setVisible(true);
				achatDirect.add(btnQuitter);
				achatDirect.add(btnRetour);
				btnRetour.setVisible(true);

			}
		});

		// actionListener pour passer au panel d'achat via ordonnance
		btnAchatOrd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAchatDirect.setVisible(false);
				btnAchatOrd.setVisible(false);
				lblDelivrance.setVisible(true);
				btnLeMdecinTraitant.setVisible(true);
				btnUnSpecialiste.setVisible(true);

			}
		});
		HistoriqueDesAchats historique = new HistoriqueDesAchats(pHistoriqueAchats);
		historique.setVisible(false);
		// actionListener pour passer au panel de l'historique d'achat
		btnHistAchats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panAcceuil.setVisible(false);
				getContentPane().add(historique);
				historique.setVisible(true);
				historique.add(btnQuitter);
				historique.add(btnRetour);
				btnRetour.setVisible(true);
			}
		});

		// actionListener pour quitter completement l'application
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		// actionListener pour revenir à la page précédente
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				achatDirect.setVisible(false);
				historique.setVisible(false);
				btnAchats.setVisible(true);
				btnHistAchats.setVisible(true);
				comboBoxOrdMedecin.setVisible(true);
				comboBoxDetailClient.setVisible(true);
				btnAchatDirect.setVisible(false);
				btnAchatOrd.setVisible(false);
				panAcceuil.setVisible(true);
				lblHistOrd.setVisible(true);
				lblChoixMedecin.setVisible(true);
				lblDetailClient.setVisible(true);
			}
		});

	}
}
