package vue;

/** 
 *  Déclarations des imports
 */
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import module.HistoriqueAchats;

/**
 * @author DuwigN
 *
 */
public class HistoriqueDesAchats extends JPanel {
	private JTable table_1;
	private JTable table;
	private JTable table_2;

	/**
	 * Constructeur de mon panel HistoriqueDes
	 * 
	 * @param pHistoriqueAchats
	 */
	public HistoriqueDesAchats(HistoriqueAchats pHistoriqueAchats) {

		setBounds(new Rectangle(0, 140, 684, 521));
		setLayout(null);
		JButton btnConfirmer = new JButton("Confirmer");

		JLabel lblHistoriqueAchat = new JLabel("Historique des achats");
		lblHistoriqueAchat.setBounds(234, 11, 195, 29);
		add(lblHistoriqueAchat);
		lblHistoriqueAchat.setFont(new Font("Serif", Font.BOLD, 20));

		int lignesTableau = 0;
		for (int i = 0; i < pHistoriqueAchats.size(); i++) {
			for (int j = 0; j < pHistoriqueAchats.getListeHistoriqueAchat().get(i).size(); j++) {
				lignesTableau++;
			}
		}

//		String[] titres = { "Medicament", "Quantité", "Client", "Plus d'informations" };
//		Object[][] donneesHistAchats = {
//				{ pHistoriqueAchats.getListeHistoriqueAchat().get(0).get(0).getMedicament().getnomMedoc(), 29, "rt",
//						"plus d'info" },
//				{ "erg", "er", 547, "plus d'info" },

//		};
		String[] titres = { "Medicament", "Quantité", "Client", "Date d'achat", "Plus d'informations" };
		Object[][] donneesHistAchats = new Object[lignesTableau / 2][5];

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		System.out.println(pHistoriqueAchats.size());
		for (int i = 0; i < pHistoriqueAchats.size(); i++) {
			for (int j = 0; j < pHistoriqueAchats.getListeHistoriqueAchat().get(i).size(); j++) {

				donneesHistAchats[j][0] = pHistoriqueAchats.getListeHistoriqueAchat().get(i).get(j).getMedicament()
						.getnomMedoc();
				donneesHistAchats[j][1] = pHistoriqueAchats.getListeHistoriqueAchat().get(i).get(j).getQuantite();
				donneesHistAchats[j][2] = pHistoriqueAchats.getListeHistoriqueAchat().get(i).get(j).getClient()
						.getNom();
				donneesHistAchats[j][3] = "";
				donneesHistAchats[j][4] = "Plus d'info.";

			}
		}

		DefaultTableModel model = new DefaultTableModel(donneesHistAchats, titres);
		JTable tableauHistOrd = new JTable();
		tableauHistOrd.setModel(model);
		tableauHistOrd.getColumnModel().getColumn(4).setCellRenderer(new TableComponent());

		JScrollPane scrollPane_1 = new JScrollPane();
		add(scrollPane_1);
		scrollPane_1.setBounds(36, 164, 610, 246);
		scrollPane_1.setViewportView(tableauHistOrd);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(21, 21, 943, 488);

	}

	public class TableComponent extends JButton implements TableCellRenderer {
		public TableComponent() {
			setOpaque(true);
		}

		/**
		 *
		 */
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			setText((value == null) ? "" : value.toString());
			return this;
		}

	}

}
