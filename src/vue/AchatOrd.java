package vue;

/** 
 *  Déclarations des imports
 */
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import module.Client;
import module.Medecin;

/**
 * @author DuwigN
 *
 */
public class AchatOrd extends JPanel {
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Constructeur de mon panel AchatOrd
	 */
	public AchatOrd() {
		// création du panel
		setBounds(new Rectangle(0, 140, 684, 521));
		setLayout(null);

		// Boutons labels et textField pour rentrer les données de l'ordonnance
		JLabel lblTitreAchatOrd = new JLabel("Achat via ordonnance");
		lblTitreAchatOrd.setBounds(256, 11, 195, 29);
		add(lblTitreAchatOrd);
		lblTitreAchatOrd.setFont(new Font("Serif", Font.BOLD, 20));

		JLabel lblDonneeOrd = new JLabel("Données de l'ordonance :");
		lblDonneeOrd.setBounds(27, 104, 147, 29);
		add(lblDonneeOrd);

		JComboBox<Client> nomDuClient = new JComboBox<>();
		nomDuClient.setBounds(102, 153, 107, 20);
		add(nomDuClient);

		JComboBox<Medecin> nomDuMedecin = new JComboBox<>();
		nomDuMedecin.setBounds(102, 208, 107, 20);
		add(nomDuMedecin);

		JLabel lblNom = new JLabel("Nom du client :");
		lblNom.setBounds(10, 149, 89, 29);
		add(lblNom);

		JLabel lblnomDuMedecin = new JLabel("Nom du Medecin :");
		lblnomDuMedecin.setBounds(10, 204, 89, 29);
		add(lblnomDuMedecin);

		JLabel lblDateDliverance = new JLabel("Date de déliverance :");
		lblDateDliverance.setBounds(27, 262, 147, 29);
		add(lblDateDliverance);

		JLabel lblJour = new JLabel("Jour :");
		lblJour.setBounds(10, 302, 89, 29);
		add(lblJour);

		JLabel lblMois = new JLabel("Mois :");
		lblMois.setBounds(10, 342, 89, 29);
		add(lblMois);

		JLabel lblAnnee = new JLabel("Année :");
		lblAnnee.setBounds(10, 382, 89, 29);
		add(lblAnnee);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(67, 302, 107, 20);
		add(textField);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(67, 342, 107, 20);
		add(textField_1);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(67, 382, 107, 20);
		add(textField_2);

		JButton btnConfirmer = new JButton("Confirmer");
		btnConfirmer.setBounds(10, 444, 100, 23);
		add(btnConfirmer);

	}
}
