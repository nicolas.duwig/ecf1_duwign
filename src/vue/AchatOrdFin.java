package vue;
/** 
 *  Déclarations des imports
 */
import java.awt.Font;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * @author DuwigN
 *
 */
public class AchatOrdFin extends JPanel {
	private JLabel lblDonneeMed;
	private JTextField textField;

	/**
	 * Constructeur de mon panel AchatOrdFin
	 * 
	 * 
	 */
	public AchatOrdFin() {
		//création du panel
		setBounds(new Rectangle(0, 140, 684, 521));
		setLayout(null);
		
		//Boutons labels et textField pour rentrer les médicaments de l'ordonnance
		JLabel lblTitreAchatOrd = new JLabel("Achat via ordonnance");
		lblTitreAchatOrd.setBounds(256, 11, 195, 29);
		add(lblTitreAchatOrd);
		lblTitreAchatOrd.setFont(new Font("Serif", Font.BOLD, 20));
		
		JLabel lblDonneeOrd;
		lblDonneeMed = new JLabel("Données du médicament :");
		lblDonneeMed.setBounds(27, 104, 147, 29);
		add(lblDonneeMed);
		
		JLabel lblNom = new JLabel("Nom :");
		lblNom.setBounds(10, 149, 89, 29);
		add(lblNom);
		
		JLabel lblDonneeOrd_1 = new JLabel("Données de l'ordonance :");
		lblDonneeOrd_1.setBounds(272, 104, 147, 29);
		add(lblDonneeOrd_1);
		
		JLabel lblQuantite = new JLabel("Quantité :");
		lblQuantite.setBounds(10, 189, 89, 29);
		add(lblQuantite);
		
		JTextField nomDuClient = new JTextField();
		nomDuClient.setBounds(70, 153, 107, 20);
		add(nomDuClient);
		nomDuClient.setColumns(10);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(70, 193, 107, 20);
		add(textField);
		
		JLabel lblClient = new JLabel("Client :");
		lblClient.setBounds(237, 149, 89, 29);
		add(lblClient);
		
		JLabel lblDelivreePar = new JLabel("Délivrée par : Dr ");
		lblDelivreePar.setBounds(237, 189, 89, 29);
		add(lblDelivreePar);
		
		JLabel lblDateDelivrance = new JLabel("Date de délivrance :");
		lblDateDelivrance.setBounds(237, 229, 107, 29);
		add(lblDateDelivrance);
		
		JLabel lblNomMedecin = new JLabel("");
		lblNomMedecin.setBounds(340, 189, 89, 29);
		add(lblNomMedecin);
		
		JLabel lblNomClient = new JLabel("");
		lblNomClient.setBounds(340, 149, 89, 29);
		add(lblNomClient);
		
		JLabel lblNomMedecin_1 = new JLabel("");
		lblNomMedecin_1.setBounds(340, 229, 89, 29);
		add(lblNomMedecin_1);
		
		JList listMed = new JList();
		listMed.setBounds(462, 95, 194, 297);
		add(listMed);
		
		JLabel lblPanier = new JLabel("Panier :");
		lblPanier.setBounds(462, 67, 147, 29);
		add(lblPanier);
		listMed.setVisible(false);
		
		JButton btnConfirmerAchat = new JButton("Confirmer l'achat");
		btnConfirmerAchat.setBounds(10, 487, 161, 23);
		add(btnConfirmerAchat);
		btnConfirmerAchat.setVisible(false);
		
		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.setBounds(10, 232, 100, 23);
		add(btnAjouter);
		btnAjouter.setVisible(false);
	}

}
