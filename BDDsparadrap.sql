CREATE DATABASE Sparadonnees;
use Sparadonnees;
drop table if exists Achat;
drop table if exists Personne;
drop table if exists Specialite;
drop table if exists Departement;
drop table if exists Mutuelle;
drop table if exists Adresse;
drop table if exists Produit;


/*==============================================================/
/ Table : Achat                                                /
/==============================================================*/
create table Achat
(
   id_Achat              int not null,
   type_Achat              boolean,
   primary key (id_Achat)
   /*foreign key (id_Prod) references Produit (id_Prod)*/
);

/*==============================================================/
/* Table : HistoriqueAchat                                                /
/==============================================================/
create table Hachat
(
   id_Hachat             int not null,
   primary key (id_Hachat)
);
/*==============================================================/
/ Table : Produit                                            /
/==============================================================*/


create table Produit

(
   id_Prod  int not null,
  date_Pro    char(10) not null,
  nom_Prod    char(50) not null,
  cat_Prod    char(50) not null,
  prix_Prod    int not null,
  quant_Prod int not null,
   primary key (id_Prod)
);



/*==============================================================/
/* Table : Personne                                              /
/==============================================================*/

create table Personne

(
   id_Pers              int not null,
   naiss_Pers           char(10) not null,
  /* secu_Pers			char(50) not null,*/
   nom_Pers				char(50) not null,
   prenom_Pers			char(50) not null,
   tel_Pers				char(50),
   email_Pers			char(50),
   primary key (id_Pers)
  /* foreign key (id_Spe) references Specialite (id_Spe),
   foreign key (id_Adresse) references Adresse (id_Adresse),
   foreign key (id_Mut) references Mutuelle (id_Mut)*/
   
);
	
/*==============================================================/
/* Table : Adresse                                       /
/==============================================================*/

create table Adresse

(
   id_Adresse           int not null,
   cp					char(10) not null,
   rue					char(50) not null,
   ville  				char(50) not null,
   
   primary key (id_Adresse)
   /*foreign key (id_Departement) references Departement (id_Departement)*/
);

/*==============================================================/
/* Table : Mutuelle                                    /
/==============================================================*/

create table Mutuelle     

(
   id_Mut	            int not null,
   lbl_Mut              char(50) not null,
   taux_Mut				int not null,
   
   primary key (id_Mut)
   /*foreign key (id_Departement) references Departement (id_Departement)*/
);


/*==============================================================/
/* Table : Departement                                     /
/==============================================================*/

create table Departement    

(
   id_Departement               int not null,
   lbl_Departement    			char(50) not null,
  
   
   primary key (id_Departement)
);


/*==============================================================/
/* Table : Specialite                                    /
/==============================================================*/

create table Specialite

(
   id_Spe               int not null,
   lbl_Spe   			char(50) not null,
  
   
   primary key (id_Spe)
);

/*==============================================================/
/ Table : Role                                            /
/==============================================================*/

create table Role
(
id_Role int not null,
lbl_Role char (50) not null,

primary key (id_Role)

);

/*==============================================================/
/* Insertion des données                                    /
/==============================================================*/


      
TRUNCATE Produit;
INSERT INTO Produit (id_Prod,date_Pro,nom_Prod,cat_Prod,prix_Prod,quant_Prod) VALUES (1,'05/01/1996',"Doliprane","Antalgique",5.00,500);
INSERT INTO Produit (id_Prod,date_Pro,nom_Prod,cat_Prod,prix_Prod,quant_Prod) VALUES (2,'25/01/1996',"Efferlagan","Antidouleur",4.50,500);
INSERT INTO Produit (id_Prod,date_Pro,nom_Prod,cat_Prod,prix_Prod,quant_Prod) VALUES (3,'15/01/1996',"Atenolol","Betabloquant",6.00,500);

Truncate Mutuelle;
INSERT INTO Mutuelle (id_Mut,lbl_Mut,taux_Mut) VALUES (1,"Harmonie Mutuelle",5.00);
INSERT INTO Mutuelle (id_Mut,lbl_Mut,taux_Mut) VALUES (2,"MGEL",5.00);
INSERT INTO Mutuelle (id_Mut,lbl_Mut,taux_Mut) VALUES (3,"CMU",5.00);

TRUNCATE Departement;
INSERT INTO  Departement (id_Departement,lbl_Departement) VALUES (1,"Meurthe et Moselle");
INSERT INTO  Departement (id_Departement,lbl_Departement) VALUES (2,"Vosges");
INSERT INTO  Departement (id_Departement,lbl_Departement) VALUES (3,"Meuse");

TRUNCATE Specialite;
INSERT INTO Specialite (id_Spe,lbl_Spe) VALUES(1,"Chirurgien");
INSERT INTO Specialite (id_Spe,lbl_Spe) VALUES(2,"Cardiologue");
INSERT INTO Specialite (id_Spe,lbl_Spe) VALUES(3,"Psycologue");

TRUNCATE Adresse;
INSERT INTO Adresse (id_Adresse,cp,rue,ville) VALUES(1,"54000","rue général Leclerc","Nancy");
INSERT INTO Adresse (id_Adresse,cp,rue,ville) VALUES(2,"55000","rue due près","Bar-le-duc");
INSERT INTO Adresse (id_Adresse,cp,rue,ville) VALUES(3,"88000","Avenue des trois vents","Epinal");

TRUNCATE Personne;
INSERT INTO Personne (id_Pers,naiss_Pers, nom_Pers,prenom_Pers,tel_Pers, email_Pers) VALUES (1,"15/07/1993","Tersin","Perence","0383679781","TersinP@gmail.com");
INSERT INTO Personne (id_Pers,naiss_Pers, nom_Pers,prenom_Pers,tel_Pers, email_Pers) VALUES (2,"14/05/1998","Dicolas","Nuwig","0385368547","dnwig@gmail.com");
INSERT INTO Personne (id_Pers,naiss_Pers, nom_Pers,prenom_Pers,tel_Pers, email_Pers) VALUES (3,"12/09/1992","Aaudon","Blan","0656235986","ABlang@gmail.com");


TRUNCATE Role; 
INSERT INTO Role ( id_Role,lbl_Role) VALUES ( 1," Clients");
INSERT INTO Role ( id_Role,lbl_Role) VALUES ( 2," Médecin");